#include"CPViewer.h"

CPViewer::CPViewer(HWND hHandler){
	// create a graphic driver from default connection 
	Handle(OpenGl_GraphicDriver) aGraphicDriver = new OpenGl_GraphicDriver(aDisplayConnection);

	aWNTWindow = new WNT_Window(hHandler);//static_cast<HWND> (this->view3d->Handle.ToPointer())
	aWNTWindow->SetVirtual(Standard_True);
	aViewer = new V3d_Viewer(aGraphicDriver);
	aContext = new AIS_InteractiveContext(aViewer);
	aViewer->SetDefaultLights();
	aViewer->SetLightOn();
	Handle(Graphic3d_Structure) aStruct = new Graphic3d_Structure(aViewer->StructureManager());
	aStruct->SetVisual(Graphic3d_TOS_SHADING);
	aView = aViewer->CreateView();
	gp_Trsf aTrsf;
	//aTrsf.SetRotation(gp_Ax1(gp_Pnt(0.0, 0.0, 0.0), gp_Dir(1.0, 1.0, 1.0)), 90.0); 
	aView->Camera()->Transform(aTrsf);
	aView->Camera()->SetProjectionType(Graphic3d_Camera::Projection_MonoLeftEye);
	//aView->Camera()->SetIOD(IODType_Absolute, 5.0); 
	//aView->SetZoom(7);//7 
	aView->SetWindow(aWNTWindow);
	aView->MustBeResized();
	aView->TriedronDisplay(Aspect_TOTP_LEFT_LOWER, Quantity_NOC_GOLD, 0.08, V3d_ZBUFFER);
	aView->Rotate(0);
	aView->StartRotation(0, 0);
	/*anAISShape = (new AIS_Shape(CPReadExample::getFromSTEP()));
	anAISShape->SetDisplayMode(1);
	anAISShape->Attributes()->SetFaceBoundaryDraw(true);
	anAISShape->SetWidth(1);
	aContext->Display(anAISShape, true);*/
	aView->FitAll();
	
}

void CPViewer::Update(){
	this->aView->MustBeResized();
	this->aView->FitAll();
	this->aView->Update();
	this->aViewer->Update();
}

void CPViewer::addShape(TopoDS_Shape shape){
	//aContext->ClearLocalContext();
	//aContext->ClearCurrents(true);
	//aContext->PurgeDisplay();
	aContext->Remove(anAISShape, true);
	//aContext->CloseAllContexts(true);
	anAISShape = new AIS_Shape(shape);
	anAISShape->SetDisplayMode(1);
	anAISShape->Attributes()->SetFaceBoundaryDraw(true);
	anAISShape->SetWidth(1);
	aContext->Display(anAISShape, true);
	
}
