#pragma once
#include<AIS_InteractiveContext.hxx>
#include<AIS_Shape.hxx>
#include<V3d_View.hxx>
#include<OpenGl_GraphicDriver.hxx>
#include<WNT_Window.hxx>

class CPViewer {
private:
	Handle(AIS_Shape) anAISShape;
	Handle(V3d_View)  aView;
	Handle(Aspect_DisplayConnection) aDisplayConnection;
	Handle(V3d_Viewer) aViewer;
	HWND hHandler;
	Handle(AIS_InteractiveContext) aContext;
	Handle(WNT_Window) aWNTWindow;

public:
	CPViewer(HWND);
	~CPViewer();
	void Update();
	void addShape(TopoDS_Shape shape);
};