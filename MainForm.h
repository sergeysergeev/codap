#pragma once
#include"CPReadExample.h"

#include<AIS_InteractiveContext.hxx>
#include<AIS_Shape.hxx>
#include<V3d_View.hxx>
#include<OpenGl_GraphicDriver.hxx>
#include<WNT_Window.hxx>
#include"CPViewer.h"
#include"CPRead.h"
#include"CPLogger.h"
#include <regex>


namespace codaP {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//

		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TabControl^  codaPTabs;
	protected:
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::Panel^  view3d;

	private: System::Windows::Forms::ToolStrip^  toolStrip1;
	private: System::Windows::Forms::TabPage^  tabPage2;

	protected:

	protected:


	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;
	private: System::Windows::Forms::ToolStripButton^  OpenFile;
	private: System::Windows::Forms::OpenFileDialog^  open3dFileDialog;

			 CPViewer *tDView;

	private: System::Windows::Forms::ToolStrip^  toolStrip2;
	private: System::Windows::Forms::ToolStripButton^  toolStripButton1;

	private: System::Windows::Forms::ToolStripButton^  toolStripButton2;
	private: System::Windows::Forms::PictureBox^  view2d;
	private: System::Windows::Forms::GroupBox^  millingCutterSetup;
	private: System::Windows::Forms::ToolStripButton^  toolStripButton3;
	private: System::Windows::Forms::ComboBox^  gerberType;
	private: System::Windows::Forms::NumericUpDown^  milingSpeed;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::NumericUpDown^  zsafe;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::NumericUpDown^  zwork;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::NumericUpDown^  zchange;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::SaveFileDialog^  exportFiles;
	private: System::Windows::Forms::NumericUpDown^  millingFeed;

	private: System::Windows::Forms::Label^  label5;





























	private: System::Windows::Forms::NumericUpDown^  offset;
	private: System::Windows::Forms::Label^  label16;
	private: System::Windows::Forms::Label^  label20;
	private: System::Windows::Forms::NumericUpDown^  milldrillDiameter;
	private: System::Windows::Forms::Label^  label18;
private: System::Windows::Forms::NumericUpDown^  cutInfeed;
private: System::Windows::Forms::Label^  label17;


			 CPRead *cpReader;
private: System::Windows::Forms::GroupBox^  slicerSetupBox;
private: System::Windows::Forms::Panel^  slicerSetup;


		 array<System::Windows::Forms::Control^>^ slicerControls;




#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MainForm::typeid));
			this->codaPTabs = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->view3d = (gcnew System::Windows::Forms::Panel());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->OpenFile = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton1 = (gcnew System::Windows::Forms::ToolStripButton());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->view2d = (gcnew System::Windows::Forms::PictureBox());
			this->millingCutterSetup = (gcnew System::Windows::Forms::GroupBox());
			this->cutInfeed = (gcnew System::Windows::Forms::NumericUpDown());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->milldrillDiameter = (gcnew System::Windows::Forms::NumericUpDown());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->label20 = (gcnew System::Windows::Forms::Label());
			this->offset = (gcnew System::Windows::Forms::NumericUpDown());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->millingFeed = (gcnew System::Windows::Forms::NumericUpDown());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->zchange = (gcnew System::Windows::Forms::NumericUpDown());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->zwork = (gcnew System::Windows::Forms::NumericUpDown());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->zsafe = (gcnew System::Windows::Forms::NumericUpDown());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->milingSpeed = (gcnew System::Windows::Forms::NumericUpDown());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->gerberType = (gcnew System::Windows::Forms::ComboBox());
			this->toolStrip2 = (gcnew System::Windows::Forms::ToolStrip());
			this->toolStripButton2 = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButton3 = (gcnew System::Windows::Forms::ToolStripButton());
			this->open3dFileDialog = (gcnew System::Windows::Forms::OpenFileDialog());
			this->exportFiles = (gcnew System::Windows::Forms::SaveFileDialog());
			this->slicerSetupBox = (gcnew System::Windows::Forms::GroupBox());
			this->slicerSetup = (gcnew System::Windows::Forms::Panel());
			this->codaPTabs->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->toolStrip1->SuspendLayout();
			this->tabPage2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->view2d))->BeginInit();
			this->millingCutterSetup->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cutInfeed))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->milldrillDiameter))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->offset))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->millingFeed))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->zchange))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->zwork))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->zsafe))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->milingSpeed))->BeginInit();
			this->toolStrip2->SuspendLayout();
			this->slicerSetupBox->SuspendLayout();
			this->SuspendLayout();
			// 
			// codaPTabs
			// 
			this->codaPTabs->Controls->Add(this->tabPage1);
			this->codaPTabs->Controls->Add(this->tabPage2);
			this->codaPTabs->Dock = System::Windows::Forms::DockStyle::Fill;
			this->codaPTabs->Location = System::Drawing::Point(0, 0);
			this->codaPTabs->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->codaPTabs->Name = L"codaPTabs";
			this->codaPTabs->SelectedIndex = 0;
			this->codaPTabs->Size = System::Drawing::Size(890, 487);
			this->codaPTabs->TabIndex = 0;
			// 
			// tabPage1
			// 
			this->tabPage1->Controls->Add(this->view3d);
			this->tabPage1->Controls->Add(this->slicerSetupBox);
			this->tabPage1->Controls->Add(this->toolStrip1);
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->tabPage1->Size = System::Drawing::Size(882, 461);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"3D";
			this->tabPage1->UseVisualStyleBackColor = true;
			// 
			// view3d
			// 
			this->view3d->AutoSize = true;
			this->view3d->Dock = System::Windows::Forms::DockStyle::Fill;
			this->view3d->Location = System::Drawing::Point(2, 33);
			this->view3d->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->view3d->Name = L"view3d";
			this->view3d->Size = System::Drawing::Size(618, 426);
			this->view3d->TabIndex = 2;
			this->view3d->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MainForm::view3d_Paint);
			this->view3d->Resize += gcnew System::EventHandler(this, &MainForm::view3d_Resize);
			// 
			// toolStrip1
			// 
			this->toolStrip1->ImageScalingSize = System::Drawing::Size(24, 24);
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->OpenFile, this->toolStripButton1 });
			this->toolStrip1->Location = System::Drawing::Point(2, 2);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->Size = System::Drawing::Size(878, 31);
			this->toolStrip1->TabIndex = 0;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// OpenFile
			// 
			this->OpenFile->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->OpenFile->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"OpenFile.Image")));
			this->OpenFile->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->OpenFile->Name = L"OpenFile";
			this->OpenFile->Size = System::Drawing::Size(28, 28);
			this->OpenFile->Text = L"toolStripButton1";
			this->OpenFile->Click += gcnew System::EventHandler(this, &MainForm::OpenFile_Click);
			// 
			// toolStripButton1
			// 
			this->toolStripButton1->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButton1.Image")));
			this->toolStripButton1->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton1->Name = L"toolStripButton1";
			this->toolStripButton1->Size = System::Drawing::Size(28, 28);
			this->toolStripButton1->Text = L"toolStripButton1";
			this->toolStripButton1->Click += gcnew System::EventHandler(this, &MainForm::toolStripButton1_Click);
			// 
			// tabPage2
			// 
			this->tabPage2->Controls->Add(this->view2d);
			this->tabPage2->Controls->Add(this->millingCutterSetup);
			this->tabPage2->Controls->Add(this->toolStrip2);
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->tabPage2->Size = System::Drawing::Size(882, 476);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"2D (gerber)";
			this->tabPage2->UseVisualStyleBackColor = true;
			// 
			// view2d
			// 
			this->view2d->Dock = System::Windows::Forms::DockStyle::Fill;
			this->view2d->ImageLocation = L"";
			this->view2d->Location = System::Drawing::Point(2, 33);
			this->view2d->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->view2d->Name = L"view2d";
			this->view2d->Size = System::Drawing::Size(617, 441);
			this->view2d->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
			this->view2d->TabIndex = 4;
			this->view2d->TabStop = false;
			// 
			// millingCutterSetup
			// 
			this->millingCutterSetup->Controls->Add(this->cutInfeed);
			this->millingCutterSetup->Controls->Add(this->label17);
			this->millingCutterSetup->Controls->Add(this->milldrillDiameter);
			this->millingCutterSetup->Controls->Add(this->label18);
			this->millingCutterSetup->Controls->Add(this->label20);
			this->millingCutterSetup->Controls->Add(this->offset);
			this->millingCutterSetup->Controls->Add(this->label16);
			this->millingCutterSetup->Controls->Add(this->millingFeed);
			this->millingCutterSetup->Controls->Add(this->label5);
			this->millingCutterSetup->Controls->Add(this->zchange);
			this->millingCutterSetup->Controls->Add(this->label4);
			this->millingCutterSetup->Controls->Add(this->zwork);
			this->millingCutterSetup->Controls->Add(this->label3);
			this->millingCutterSetup->Controls->Add(this->zsafe);
			this->millingCutterSetup->Controls->Add(this->label2);
			this->millingCutterSetup->Controls->Add(this->milingSpeed);
			this->millingCutterSetup->Controls->Add(this->label1);
			this->millingCutterSetup->Controls->Add(this->gerberType);
			this->millingCutterSetup->Dock = System::Windows::Forms::DockStyle::Right;
			this->millingCutterSetup->Location = System::Drawing::Point(619, 33);
			this->millingCutterSetup->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->millingCutterSetup->Name = L"millingCutterSetup";
			this->millingCutterSetup->Padding = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->millingCutterSetup->Size = System::Drawing::Size(261, 441);
			this->millingCutterSetup->TabIndex = 3;
			this->millingCutterSetup->TabStop = false;
			this->millingCutterSetup->Text = L"��������� �������";
			// 
			// cutInfeed
			// 
			this->cutInfeed->DecimalPlaces = 2;
			this->cutInfeed->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->cutInfeed->Location = System::Drawing::Point(8, 320);
			this->cutInfeed->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->cutInfeed->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 20000, 0, 0, 0 });
			this->cutInfeed->Name = L"cutInfeed";
			this->cutInfeed->Size = System::Drawing::Size(151, 20);
			this->cutInfeed->TabIndex = 26;
			this->cutInfeed->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 2, 0, 0, 0 });
			// 
			// label17
			// 
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(5, 304);
			this->label17->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(152, 13);
			this->label17->TabIndex = 25;
			this->label17->Text = L"������������ ������� (��)";
			// 
			// milldrillDiameter
			// 
			this->milldrillDiameter->DecimalPlaces = 2;
			this->milldrillDiameter->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->milldrillDiameter->Location = System::Drawing::Point(8, 284);
			this->milldrillDiameter->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->milldrillDiameter->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 20000, 0, 0, 0 });
			this->milldrillDiameter->Name = L"milldrillDiameter";
			this->milldrillDiameter->Size = System::Drawing::Size(151, 20);
			this->milldrillDiameter->TabIndex = 24;
			this->milldrillDiameter->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 25, 0, 0, 65536 });
			// 
			// label18
			// 
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(5, 268);
			this->label18->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(121, 13);
			this->label18->TabIndex = 23;
			this->label18->Text = L"�������� ����� (��)";
			// 
			// label20
			// 
			this->label20->AutoSize = true;
			this->label20->Location = System::Drawing::Point(5, 13);
			this->label20->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label20->Name = L"label20";
			this->label20->Size = System::Drawing::Size(42, 13);
			this->label20->TabIndex = 22;
			this->label20->Text = L"�����";
			// 
			// offset
			// 
			this->offset->DecimalPlaces = 2;
			this->offset->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->offset->Location = System::Drawing::Point(8, 248);
			this->offset->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->offset->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 20000, 0, 0, 0 });
			this->offset->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, System::Int32::MinValue });
			this->offset->Name = L"offset";
			this->offset->Size = System::Drawing::Size(151, 20);
			this->offset->TabIndex = 15;
			this->offset->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 5, 0, 0, 65536 });
			// 
			// label16
			// 
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(5, 231);
			this->label16->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(127, 13);
			this->label16->TabIndex = 14;
			this->label16->Text = L"�������� �� ���� (��)";
			// 
			// millingFeed
			// 
			this->millingFeed->DecimalPlaces = 2;
			this->millingFeed->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->millingFeed->Location = System::Drawing::Point(8, 211);
			this->millingFeed->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->millingFeed->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 20000, 0, 0, 0 });
			this->millingFeed->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, System::Int32::MinValue });
			this->millingFeed->Name = L"millingFeed";
			this->millingFeed->Size = System::Drawing::Size(151, 20);
			this->millingFeed->TabIndex = 13;
			this->millingFeed->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 25, 0, 0, 0 });
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(5, 195);
			this->label5->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(190, 13);
			this->label5->TabIndex = 12;
			this->label5->Text = L"�������������� �������� (��/���)";
			// 
			// zchange
			// 
			this->zchange->DecimalPlaces = 2;
			this->zchange->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->zchange->Location = System::Drawing::Point(8, 175);
			this->zchange->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->zchange->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 20000, 0, 0, 0 });
			this->zchange->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, System::Int32::MinValue });
			this->zchange->Name = L"zchange";
			this->zchange->Size = System::Drawing::Size(151, 20);
			this->zchange->TabIndex = 11;
			this->zchange->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 25, 0, 0, 0 });
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(5, 159);
			this->label4->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(175, 13);
			this->label4->TabIndex = 10;
			this->label4->Text = L"������ ����� ����������� (��)";
			// 
			// zwork
			// 
			this->zwork->DecimalPlaces = 2;
			this->zwork->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->zwork->Location = System::Drawing::Point(8, 138);
			this->zwork->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->zwork->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 20000, 0, 0, 0 });
			this->zwork->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, System::Int32::MinValue });
			this->zwork->Name = L"zwork";
			this->zwork->Size = System::Drawing::Size(151, 20);
			this->zwork->TabIndex = 9;
			this->zwork->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 6, 0, 0, -2147352576 });
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(5, 122);
			this->label3->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(114, 13);
			this->label3->TabIndex = 8;
			this->label3->Text = L"������� ������ (��)";
			// 
			// zsafe
			// 
			this->zsafe->DecimalPlaces = 2;
			this->zsafe->Increment = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 65536 });
			this->zsafe->Location = System::Drawing::Point(8, 102);
			this->zsafe->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->zsafe->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 20000, 0, 0, 0 });
			this->zsafe->Name = L"zsafe";
			this->zsafe->Size = System::Drawing::Size(151, 20);
			this->zsafe->TabIndex = 7;
			this->zsafe->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 21, 0, 0, 65536 });
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(5, 86);
			this->label2->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(133, 13);
			this->label2->TabIndex = 6;
			this->label2->Text = L"���������� ������ (��)";
			// 
			// milingSpeed
			// 
			this->milingSpeed->Location = System::Drawing::Point(8, 66);
			this->milingSpeed->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->milingSpeed->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 20000, 0, 0, 0 });
			this->milingSpeed->Name = L"milingSpeed";
			this->milingSpeed->Size = System::Drawing::Size(151, 20);
			this->milingSpeed->TabIndex = 5;
			this->milingSpeed->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 8000, 0, 0, 0 });
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(5, 51);
			this->label1->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(154, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"�������� �������� (��/���)";
			// 
			// gerberType
			// 
			this->gerberType->DisplayMember = L"1";
			this->gerberType->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->gerberType->FormattingEnabled = true;
			this->gerberType->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"front", L"back", L"drill", L"outline" });
			this->gerberType->Location = System::Drawing::Point(8, 30);
			this->gerberType->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->gerberType->Name = L"gerberType";
			this->gerberType->Size = System::Drawing::Size(153, 21);
			this->gerberType->TabIndex = 2;
			this->gerberType->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::gerberType_SelectedIndexChanged);
			// 
			// toolStrip2
			// 
			this->toolStrip2->ImageScalingSize = System::Drawing::Size(24, 24);
			this->toolStrip2->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->toolStripButton2,
					this->toolStripButton3
			});
			this->toolStrip2->Location = System::Drawing::Point(2, 2);
			this->toolStrip2->Name = L"toolStrip2";
			this->toolStrip2->Size = System::Drawing::Size(878, 31);
			this->toolStrip2->TabIndex = 2;
			this->toolStrip2->Text = L"2D (Gerber)";
			// 
			// toolStripButton2
			// 
			this->toolStripButton2->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButton2.Image")));
			this->toolStripButton2->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton2->Name = L"toolStripButton2";
			this->toolStripButton2->Size = System::Drawing::Size(28, 28);
			this->toolStripButton2->Text = L"toolStripButton2";
			this->toolStripButton2->Click += gcnew System::EventHandler(this, &MainForm::toolStripButton2_Click);
			// 
			// toolStripButton3
			// 
			this->toolStripButton3->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButton3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButton3.Image")));
			this->toolStripButton3->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButton3->Name = L"toolStripButton3";
			this->toolStripButton3->Size = System::Drawing::Size(28, 28);
			this->toolStripButton3->Text = L"toolStripButton3";
			this->toolStripButton3->Click += gcnew System::EventHandler(this, &MainForm::toolStripButton3_Click);
			// 
			// slicerSetupBox
			// 
			this->slicerSetupBox->Controls->Add(this->slicerSetup);
			this->slicerSetupBox->Dock = System::Windows::Forms::DockStyle::Right;
			this->slicerSetupBox->Location = System::Drawing::Point(620, 33);
			this->slicerSetupBox->Margin = System::Windows::Forms::Padding(2);
			this->slicerSetupBox->Name = L"slicerSetupBox";
			this->slicerSetupBox->Padding = System::Windows::Forms::Padding(2);
			this->slicerSetupBox->Size = System::Drawing::Size(260, 426);
			this->slicerSetupBox->TabIndex = 1;
			this->slicerSetupBox->TabStop = false;
			this->slicerSetupBox->Text = L"�������";
			// 
			// slicerSetup
			// 
			this->slicerSetup->AutoScroll = true;
			this->slicerSetup->Dock = System::Windows::Forms::DockStyle::Fill;
			this->slicerSetup->Location = System::Drawing::Point(2, 15);
			this->slicerSetup->Name = L"slicerSetup";
			this->slicerSetup->Size = System::Drawing::Size(256, 409);
			this->slicerSetup->TabIndex = 0;
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(890, 487);
			this->Controls->Add(this->codaPTabs);
			this->Margin = System::Windows::Forms::Padding(2, 2, 2, 2);
			this->Name = L"MainForm";
			this->Text = L"CodaP";
			this->Load += gcnew System::EventHandler(this, &MainForm::MainForm_Load);
			this->ResizeEnd += gcnew System::EventHandler(this, &MainForm::MainForm_ResizeEnd);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MainForm::MainForm_Paint);
			this->codaPTabs->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->tabPage1->PerformLayout();
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			this->tabPage2->ResumeLayout(false);
			this->tabPage2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->view2d))->EndInit();
			this->millingCutterSetup->ResumeLayout(false);
			this->millingCutterSetup->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->cutInfeed))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->milldrillDiameter))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->offset))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->millingFeed))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->zchange))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->zwork))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->zsafe))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->milingSpeed))->EndInit();
			this->toolStrip2->ResumeLayout(false);
			this->toolStrip2->PerformLayout();
			this->slicerSetupBox->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void MainForm_Load(System::Object^  sender, System::EventArgs^  e) {
		this->tDView = new CPViewer(static_cast<HWND> (this->view3d->Handle.ToPointer()));
		this->cpReader = new CPRead();
		this->gerberType->SelectedIndex = 1;
		ifstream config;
		config.open("config/config.txt");
		int count;
		config >> count;
		int startYPoint = 10;
		std::string text;
		std::getline(config, text);
		int labelHeight = 15;
		System::Windows::Forms::Control^ test;
		
		for (int i = 0; i < count; i++){
			System::Windows::Forms::Label^ testLabel = (gcnew System::Windows::Forms::Label());
			std::getline(config, text); // get Title
			//config.getline(text.c_str(), 64);
		//	config >> text;
			std::string cliArg;
			std::getline(config, cliArg); // get Cli Arg name

			std::string fieldType;
			std::getline(config, fieldType); // get Field Type
			if (fieldType.compare("NumericUpDown") == 0) {
				testLabel->Text = gcnew System::String(text.c_str());
				this->slicerSetup->Controls->Add(testLabel);
				testLabel->Location = System::Drawing::Point(3, startYPoint);
				testLabel->Size = System::Drawing::Size(350, labelHeight);
				startYPoint += labelHeight + 1;
				test = (gcnew System::Windows::Forms::NumericUpDown());
				int decimalPlaces;
				config >> decimalPlaces; // get Decimal Places for 
				//std::getline(config, text);
				float defaultValue;
				config >> defaultValue;
				//std::getline(config, text);
				float increment;
				config >> increment;
				float minimum;
				config >> minimum;
				float maximum;
				config >> maximum;
				//std::getline(config, text);
				((System::Windows::Forms::NumericUpDown^) test)->DecimalPlaces = decimalPlaces;
				((System::Windows::Forms::NumericUpDown^) test)->Minimum = (Decimal)minimum;
				((System::Windows::Forms::NumericUpDown^) test)->Maximum = (Decimal)maximum;
				((System::Windows::Forms::NumericUpDown^) test)->Value = (Decimal)defaultValue;
				((System::Windows::Forms::NumericUpDown^) test)->Increment = (Decimal)increment;
				((System::Windows::Forms::NumericUpDown^) test)->Name = gcnew System::String(cliArg.c_str());
				std::getline(config, text);
			}
			if (fieldType.compare("CheckBox") == 0) {
				test = gcnew System::Windows::Forms::CheckBox();
				std::string checkBoxValue;
				std::getline(config, checkBoxValue);
				((System::Windows::Forms::CheckBox^) test)->Text = gcnew System::String(text.c_str());
				test->Name = gcnew System::String(cliArg.c_str());
				if (checkBoxValue.compare("true") == 0) {
					((System::Windows::Forms::CheckBox^) test)->Checked = true;
				}
				else {
					((System::Windows::Forms::CheckBox^) test)->Checked = false;
				}
			}
			if (fieldType.compare("ComboBox") == 0) {
				testLabel->Text = gcnew System::String(text.c_str());
				this->slicerSetup->Controls->Add(testLabel);
				testLabel->Location = System::Drawing::Point(3, startYPoint);
				testLabel->Size = System::Drawing::Size(250, labelHeight);
				startYPoint += labelHeight + 1;
				test = gcnew System::Windows::Forms::ComboBox();
				int elemetsCount;
				config >> elemetsCount;
				std::getline(config, text);
				for (int elemNum = 0; elemNum < elemetsCount; elemNum++) {
					std::getline(config, text);
					((System::Windows::Forms::ComboBox^)test)->Items->Add(gcnew System::String(text.c_str()));
				}
				((System::Windows::Forms::ComboBox^)test)->SelectedIndex = 0;
				((System::Windows::Forms::ComboBox^)test)->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
				((System::Windows::Forms::ComboBox^) test)->Name = gcnew System::String(cliArg.c_str());
				((System::Windows::Forms::ComboBox^) test)->Size = System::Drawing::Size(350, labelHeight);
			}
			//MessageBox::Show(this, test->GetType()->ToString(), "test", MessageBoxButtons::OK);
			this->slicerSetup->Controls->Add(test);
			test->Location = System::Drawing::Point(3, startYPoint);
			test->Size = System::Drawing::Size(150, 20);
			startYPoint += 25;
		}

		//System::Windows::Forms::NumericUpDown
		
		//this->tDView->addShape(this->cpReader->IdfRead());
	}
	private: System::Void MainForm_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
		/*
		 */
	}
	private: System::Void view3d_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
		this->tDView->Update();
	}
	private: System::Void view3d_Resize(System::Object^  sender, System::EventArgs^  e) {
		
		//this->tDView->Update();
	}
	private: System::Void MainForm_ResizeEnd(System::Object^  sender, System::EventArgs^  e) {
		if (this->tDView != NULL){
			this->tDView->Update();
		}
	}

	
	private: System::Void OpenFile_Click(System::Object^  sender, System::EventArgs^  e) {
		this->open3dFileDialog->Filter = "������� STEP, IGES, IDF, STL (*.step *.stp *.iges *.igs *.brd *.stl)|*.step;*.stp;*.iges;*.igs;*.brd;*.stl";
		if (this->open3dFileDialog->ShowDialog() == ::System::Windows::Forms::DialogResult::OK){
			String^ fullpath = this->open3dFileDialog->FileName->ToString();
			int lastPointIndex = fullpath->LastIndexOf(".");
			String^ filename = fullpath->Substring(lastPointIndex, fullpath->Length - lastPointIndex);
			this->cpReader->setFilename((Standard_CString)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(this->open3dFileDialog->FileName).ToPointer());
			bool success = false;
			filename = filename->ToLower();
			
			if (filename->Equals(".step") || filename->Equals(".stp")){
				try{
					this->tDView->addShape(this->cpReader->StepRead());
				} catch (std::exception e){
					MessageBox::Show(this, "�� ����� ������ � ������ ��������� ������.", "������", MessageBoxButtons::OK);
				}
				success = true;
				
			}
			if (filename->Equals(".iges") || filename->Equals(".igs")){
				this->tDView->addShape(this->cpReader->IgesRead());
				success = true;
			}
			if (filename->Equals(".stl")){
				this->tDView->addShape(this->cpReader->StlRead());
				success = true;
			}
			if (filename->Equals(".brd")){
				this->tDView->addShape(this->cpReader->IdfRead());
				success = true;
			}
			if (!success){
				MessageBox::Show(this, "���� � ��������� ����������� �� ��������������.", "������", MessageBoxButtons::OK);
			}
			
			this->tDView->Update();
		}
	}
private: System::Void view2d_Click(System::Object^  sender, System::EventArgs^  e) {
	//this->view2d->Lo;
}
private: System::Void toolStripButton1_Click(System::Object^  sender, System::EventArgs^  e) {
	this->exportFiles->AddExtension = true;
	this->exportFiles->Filter = "gcode files (*.gcode)|*.gcode";
	if (this->exportFiles->ShowDialog() == ::System::Windows::Forms::DialogResult::OK){
		String^ outputFile = this->exportFiles->FileName->Replace("\\", "/");
		outputFile = outputFile->Substring(0, outputFile->LastIndexOf("/"));
		String^ fileName = this->exportFiles->FileName->Substring(outputFile->Length + 1, (this->exportFiles->FileName->Length - outputFile->Length - 1));
		String^ command = "extensions/slicer/slic3r-console.exe ";  
		for (int controlNum = 0; controlNum < this->slicerSetup->Controls->Count; controlNum++) {
			System::Windows::Forms::Control^ currentControl = this->slicerSetup->Controls[controlNum];
			if (System::String::Compare(currentControl->GetType()->ToString(),"System.Windows.Forms.NumericUpDown",false) == 0) {
				String^ flag = String::Format("{0} {1} ", 
					((System::Windows::Forms::NumericUpDown^)currentControl)->Name, 
					currentControl->Text->ToString()->Replace(",", "."));
				command += flag;
			}
			if (System::String::Compare(currentControl->GetType()->ToString(), "System.Windows.Forms.CheckBox", false) == 0) {
				if (((System::Windows::Forms::CheckBox^)currentControl)->Checked) {
					command += String::Format("{0} ", currentControl->Name);
				}
			}
			if (System::String::Compare(currentControl->GetType()->ToString(), "System.Windows.Forms.ComboBox", false) == 0) {
				String^ flag = String::Format("{0} {1} ",
					((System::Windows::Forms::ComboBox^)currentControl)->Name,
					((System::Windows::Forms::ComboBox^)currentControl)->SelectedItem);
				command += flag;
			}
			//System::Windows::Forms::ComboBox
		}
		command += String::Format("-o {0} --output-filename-format {1} tmp/preprocessed.stl", outputFile, fileName);
		//MessageBox::Show(this, command, "test", MessageBoxButtons::OK);
		char* cCommand = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(command->ToString()).ToPointer();
		CPLogger::Print(cCommand);
		CPAppExecute::PcbExec(cCommand);
	}
}
private: System::Void toolStripButton2_Click(System::Object^  sender, System::EventArgs^  e) {
	this->open3dFileDialog->Filter = "��� ������� (*.*)|*.*";
	if (this->open3dFileDialog->ShowDialog() == ::System::Windows::Forms::DialogResult::OK){
		String^ name = "tmp/outp0_original_back.png";
		remove((char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(name->ToString()).ToPointer());
		String^ file = this->open3dFileDialog->FileName->Replace("\\", "/");
		String^ type = this->gerberType->SelectedItem->ToString();
		String^ command = "extensions/gerber/pcb2gcode.exe --metric --zsafe 2 --zwork -0.06 --milldrill 1 --cutter-diameter 0.1 --offset 0.2 --zchange 25 --mill-feed 100 --mill-speed 5000 --back" + " " + file + " --output-dir tmp/ --no-export";
		char* cCommand = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(command->ToString()).ToPointer();
		CPLogger::Print(cCommand);
		CPAppExecute::PcbExec(cCommand);
		char * cName = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(name->ToString()).ToPointer();
		try{
			System::IO::Stream ^stream = System::IO::File::Open(name, System::IO::FileMode::Open);
			Bitmap ^img = gcnew Bitmap(stream);
			this->view2d->Image = img;
			stream->Close();
		}
		catch (System::IO::IOException ^e){
			String^ message = "�� ����� ��������� ����� ��������� ������.";
			String^ caption = "������";
			MessageBoxButtons buttons = MessageBoxButtons::OK;
			System::Windows::Forms::DialogResult result;
			result = MessageBox::Show(this, message, caption, buttons);
		}	
	}
}
	private: System::Void toolStripButton3_Click(System::Object^  sender, System::EventArgs^  e) {
		this->exportFiles->AddExtension = true;
		this->exportFiles->Filter = "gcode files (*.ngc)|*.ngc";
		if (this->exportFiles->ShowDialog() == ::System::Windows::Forms::DialogResult::OK){
			String^ type = this->gerberType->SelectedItem->ToString();
			String^ inputFile = this->open3dFileDialog->FileName->Replace("\\", "/");
			String^ outputFile = this->exportFiles->FileName->Replace("\\", "/");
			outputFile = outputFile->Substring(0, outputFile->LastIndexOf("/"));
			String^ fileName = this->exportFiles->FileName->Substring(outputFile->Length + 1, (this->exportFiles->FileName->Length - outputFile->Length - 1));
			String^ command = "";
			bool success = false;
			//TODO: make a bit refactoring
			if (type->Equals("back") || type->Equals("front")) {
				String^ additional;
				if (type->Equals("back")){
					additional = gcnew String(CPAppExecute::AdditionalArgs("config/back.config").c_str());
				} else {
					additional = gcnew String(CPAppExecute::AdditionalArgs("config/front.config").c_str());
				}
				command = String::Format("extensions/gerber/pcb2gcode.exe --metric --zsafe {0} --zwork {1}  --zchange {2} --mill-feed {3} --mill-speed {4} --{5} {6} --output-dir {7} --{5}-output {8} --offset {9} {10}",
				this->zsafe->Text->ToString()->Replace(",", "."),
				this->zwork->Text->ToString()->Replace(",", "."),
				this->zchange->Text->ToString()->Replace(",", "."),
				this->millingFeed->Text->ToString()->Replace(",", "."),
				this->milingSpeed->Text->Replace(",", "."),
				type, inputFile, outputFile, fileName, 
				this->offset->Text->ToString()->Replace(",", "."),
				additional
				);
				success = true;
			}
			if (type->Equals("drill")){
				String^ additional = gcnew String(CPAppExecute::AdditionalArgs("config/drill.config").c_str());
				command = String::Format("extensions/gerber/pcb2gcode.exe --metric --drill {0} --zsafe {1} --zchange {2} --zdrill {3} --drill-feed {4} --drill-speed {5} --output-dir {6} --drill-output {8} {7}",
					inputFile,
					this->zsafe->Text->ToString()->Replace(",", "."),
					this->zchange->Text->ToString()->Replace(",", "."),
					this->zwork->Text->ToString()->Replace(",", "."),
					this->millingFeed->Text->ToString()->Replace(",", "."),
					this->milingSpeed->Text->ToString()->Replace(",", "."),
					outputFile,
					additional,
					fileName
					);
				success = true;
			}
			if (type->Equals("outline")){
				String^ additional = gcnew String(CPAppExecute::AdditionalArgs("config/outline.config").c_str());
				command = String::Format("extensions/gerber/pcb2gcode.exe --metric --output-dir {0} --zsafe {1} --zchange {2} --zcut {3} --cutter-diameter {4}"+
					" --cut-feed {5} --cut-speed {6} --cut-infeed {7} --outline {8} {9} --outline-output {10}",
					outputFile,
					this->zsafe->Text->ToString()->Replace(",", "."),
					this->zchange->Text->ToString()->Replace(",", "."),
					this->zwork->Text->ToString()->Replace(",", "."),
					this->milldrillDiameter->Text->ToString()->Replace(",", "."),
					this->millingFeed->Text->ToString()->Replace(",", "."),
					this->milingSpeed->Text->ToString()->Replace(",", "."),
					this->cutInfeed->Text->ToString()->Replace(",", "."),
					inputFile,
					additional,
					fileName
					);
				success = true;
			}
			if (success) {
				char* cCommand = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(command->ToString()).ToPointer();
				CPLogger::Print(cCommand);
				CPAppExecute::PcbExec(cCommand);
			}
			else {
				MessageBox::Show(this, "�� ����� ��������� ����� ��������� ������.", "������", MessageBoxButtons::OK);
			}

	}
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	this->tDView->addShape(this->cpReader->IdfRead());
}
private: System::Void gerberType_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	this->offset->Enabled = this->gerberType->SelectedItem->ToString()->Equals("back") || this->gerberType->SelectedItem->ToString()->Equals("front");
	this->cutInfeed->Enabled = this->gerberType->SelectedItem->ToString()->Equals("outline");
	this->milldrillDiameter->Enabled = this->gerberType->SelectedItem->ToString()->Equals("outline");
}
};
}
