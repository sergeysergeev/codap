#include"CPRead.h"

CPRead::CPRead(){
	this->filename = "";
}

Standard_CString CPRead::getFilename(){
	return this->filename;
}

void CPRead::setFilename(Standard_CString fname){
	this->filename = fname;
}

TopoDS_Shape CPRead::StepRead(){
	STEPControl_Reader reader;
	CPLogger::Instance()->myfile << this->filename << endl;
	IFSelect_ReturnStatus status = reader.ReadFile(this->filename);//
	if (status != IFSelect_RetDone) {
		CPLogger::Instance()->myfile << status;
		throw new std::exception();
	}
	reader.TransferRoots();
	TopoDS_Shape aShape = reader.OneShape();
	
	this->exportTmpStl(aShape);

	return aShape;
}

TopoDS_Shape CPRead::StlRead(){
	TopoDS_Shape aShape;
	StlAPI_Reader reader;
	reader.Read(aShape, this->filename);

	this->exportTmpStl(aShape);
	
	return aShape;
}

TopoDS_Shape CPRead::IgesRead(){
	IGESControl_Reader reader;
	IFSelect_ReturnStatus status = reader.ReadFile(this->filename);//
	if (status != IFSelect_RetDone) {
		throw new std::exception();
	}
	reader.TransferRoots();
	TopoDS_Shape aShape = reader.OneShape();

	this->exportTmpStl(aShape);

	return aShape;
}

TopoDS_Shape CPRead::IdfRead(){
	TopoDS_Shape aShape = (new CPIdf_Reader((char*)this->filename))->Read();
	this->exportTmpStl(aShape);
	return aShape;
}

void CPRead::exportTmpStl(TopoDS_Shape aShape){
	StlAPI_Writer stlWriter = StlAPI_Writer();
	BRepMesh_IncrementalMesh aMesh(aShape, 0.1);
	stlWriter.ASCIIMode() = Standard_True;
	stlWriter.Write(aShape, "tmp/preprocessed.stl");
}