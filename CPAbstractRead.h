#include<TopoDS_Shape.hxx>

class CPAbstractRead {
public:
	virtual TopoDS_Shape StepRead();
	virtual TopoDS_Shape IgesRead();
	virtual TopoDS_Shape StlRead();
protected:
	Standard_CString filename;
};