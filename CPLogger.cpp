#include"CPLogger.h"
CPLogger* CPLogger::_instance = 0;
CPLogger::CPLogger(){
	myfile.open("log.txt");
}
CPLogger::~CPLogger(){
	myfile.close();
}
CPLogger* CPLogger::Instance() {
	if (_instance == 0) {
		_instance = new CPLogger;
	}
	return _instance;
}
void CPLogger::Print(const char* mes){
	//PrintNow();
	CPLogger::Instance()->myfile << mes << std::endl;
}
void CPLogger::PrintNow(){
	time_t now = time(0);
	tm *ltm = localtime(&now);
	CPLogger::Instance()->myfile
		<< 1900 + ltm->tm_year << "-" << 1 + ltm->tm_mon << "-" << ltm->tm_mday << " "
		<< 1 + ltm->tm_hour << ":" << 1 + ltm->tm_min << ":" << 1 + ltm->tm_sec << " ";
}