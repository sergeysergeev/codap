#pragma once
#include<fstream>
#include <ctime>

class CPLogger {
public:
	static CPLogger* Instance();
	static void Print(const char* mes);
	std::ofstream myfile;
	static void PrintNow();
protected:
	CPLogger();
	~CPLogger();
private:
	static CPLogger* _instance;
	
};
