#include <Windows.h>
#include <process.h>
#include<exception>
#include <iostream>
#include <fstream>
#include <string>

class CPAppExecute 
{
public:
	CPAppExecute();
	static void Exec();
	static void PcbPng();
	static void PcbProcess(char* zsafe, char* zwork, char* offset, char* zchange, char* millFeed, char* millSpeed, char* type, char* inputFile, char* outputFile);
	static void PcbExec(char command[]);
	// --zsafe 2 --zwork -0.06 --offset 0.2 --zchange 25 --mill-feed 100 --mill-speed 5000 --back ex.gbl
	static std::string AdditionalArgs(char * filename);
private:
	static VOID startup(char arg[]);

};