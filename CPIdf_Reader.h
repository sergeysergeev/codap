#include<BRepPrimAPI_MakeTorus.hxx>
#include<BRepAlgoAPI_Fuse.hxx>
#include<BRepBuilderAPI_MakeEdge.hxx>
#include<BRepBuilderAPI_MakeWire.hxx>
#include<BRepBuilderAPI_MakeFace.hxx>
#include<BRepPrimAPI_MakePrism.hxx>
#include<Geom_TrimmedCurve.hxx>
#include<GC_MakeArcOfCircle.hxx>
#include<BRepAlgoAPI_Cut.hxx>
#include <gp_Circ.hxx>
#include <gp_Ax2.hxx>
#include <GC_MakeCircle.hxx>
#include <iostream>
#include <vector>
#include<math.h>
#include"CPLogger.h"

class CPIdf_Reader {
public:
	CPIdf_Reader(char *fname){
		this->filename = fname;
	}
	TopoDS_Shape Read(){
		std::ifstream input;
		input.open(this->filename);
		std::string line;
		TopoDS_Shape resShape;
		while (std::getline(input, line)){
			if (line.compare(0, 7, std::string(".HEADER")) == 0){
				getHead(input);
			}
			if (line.compare(0, 14, std::string(".BOARD_OUTLINE")) == 0){
				resShape = getBoard(input, std::string(".END_BOARD_OUTLINE"));
			}
			if (line.compare(0, 14, std::string(".DRILLED_HOLES")) == 0) {
				makeDrill(input, resShape);
			}
			//PLACE_OUTLINE
			if (line.compare(0, 14, std::string(".PLACE_OUTLINE")) == 0) {
				makeOutLine(input, resShape);
			}

		} 
		return resShape;
	}
private:
	char *filename;
	gp_Vec aPrismVec;
	double thickness;
	float isMetric = true;

	void makeMMFromInput(Standard_Real &x){
		if (!this->isMetric){
			x *= 0.0254;
		}
	}

	void getHead(std::ifstream &input){
		std::string line;
		std::getline(input, line);
		std::getline(input, line);
		std::stringstream ss(line);
		std::string name, metricType;
		ss >> name >> metricType;
		this->isMetric = (metricType.compare("MM") == 0);
		if (this->isMetric) {
			CPLogger::Instance()->myfile << "It's metric IDF \n";
		}
		else {
			CPLogger::Instance()->myfile << "It's NOT metric IDF \n";
		}
	}
	void makeDrill(std::ifstream &input, TopoDS_Shape &resShape){
		std::string line;
		while (std::getline(input, line) && line.compare(0, 18, std::string(".END_DRILLED_HOLES")) != 0) {
			std::stringstream ss(line);
			double radius, x, y;
			CPLogger::Print(line.c_str());
			ss >> radius >> x >> y;
			makeMMFromInput(radius);
			makeMMFromInput(x);
			makeMMFromInput(y);
			gp_Pnt point(x, y, 0);
			gp_Dir neckAxis = gp::DZ();
			gp_Ax2 neckAx2(point, neckAxis);
			gp_Circ circ(neckAx2, radius / 2);
			gp_Vec aPrismVec(0, 0, this->thickness);
			resShape = BRepAlgoAPI_Cut(resShape, BRepPrimAPI_MakePrism(BRepBuilderAPI_MakeFace(BRepBuilderAPI_MakeWire(BRepBuilderAPI_MakeEdge(circ))), aPrismVec).Shape());
			CPLogger::Instance()->myfile << "diam " << radius << " X " << x << " Y " << y << endl;
		}
	}
	void makeOutLine(std::ifstream &input, TopoDS_Shape &resShape){
		std::string line;
		while (std::getline(input, line) && line.compare(0, 18, std::string(".END_PLACE_OUTLINE"))) {
			//TODO: add outline
		}
	}

	TopoDS_Shape getBoard(std::ifstream &input, std::string block){
		std::string line;
		std::getline(input, line);
		std::stringstream ss1(line);
		std::vector<gp_Pnt> board;
		std::vector<Standard_Real> angles;
		std::vector<int> loopLabels;
		std::vector<TopoDS_Edge> edges;

		ss1 >> this->thickness;
		makeMMFromInput(this->thickness);
		int loopLabel = -1;
		gp_Vec aPrismVec(0, 0, this->thickness);
		TopoDS_Shape resShape;
		while (std::getline(input, line) && line.compare(0, block.length(), block)){
			if (line[0] == '#'){
				continue;
			}
			Standard_Real x, y, angle;
			std::stringstream ss(line);
			ss >> loopLabel >> x >> y >> angle;
			makeMMFromInput(x);
			makeMMFromInput(y);
			gp_Pnt aPnt(x, y, 0);
			loopLabels.push_back(loopLabel);
			angles.push_back(angle);
			board.push_back(aPnt);
		}
		loopLabels.push_back(loopLabel+1);
		int currentLoop = 0;
		for (int i = 1; i < loopLabels.size(); i++){
			//CPLogger::Instance()->myfile << " iterator " << i;
			if (currentLoop == loopLabels[i]){
			//	CPLogger::Instance()->myfile << " cloop " << currentLoop << endl;
				Standard_Real angle = angles[i];
				if (angle != 0 && angle != 360) {
					edges.push_back(BRepBuilderAPI_MakeEdge(makeArkOfCircle(board[i - 1], board[i], angle)));
				}
				else if (abs(angle) == 360){
					gp_Pnt circleCenter(board[i - 1].X(), board[i - 1].Y(), 0);
					gp_Dir neckAxis = gp::DZ();
					gp_Ax2 neckAx2(circleCenter, neckAxis);
					Standard_Real circleRadius = sqrt((board[i - 1].X() - board[i].X())*(board[i - 1].X() - board[i].X()) + (board[i - 1].Y() - board[i].Y())*(board[i - 1].Y() - board[i].Y()));
					gp_Circ circ(neckAx2, circleRadius);
					edges.push_back(BRepBuilderAPI_MakeEdge(circ));
				}
				else {
					edges.push_back(BRepBuilderAPI_MakeEdge(board[i - 1], board[i]));
				}
			}
			else {
				BRepBuilderAPI_MakeWire wire;
				for (int i = 0; i < edges.size(); i++) {
					wire.Add(edges[i]);
				}
				CPLogger::Instance()->myfile << " else if \n";
				if (currentLoop == 0){
					resShape = BRepPrimAPI_MakePrism(BRepBuilderAPI_MakeFace(wire), aPrismVec).Shape();
				}
				else {
					resShape = BRepAlgoAPI_Cut(resShape, BRepPrimAPI_MakePrism(BRepBuilderAPI_MakeFace(wire), aPrismVec).Shape());
				}
				currentLoop = loopLabels[i];
				edges.clear();
			}
		}
		
		//resShape = BRepPrimAPI_MakePrism(BRepBuilderAPI_MakeFace(wire), aPrismVec).Shape();
		return resShape;
	}

	Handle(Geom_TrimmedCurve) makeArkOfCircle(gp_Pnt p1, gp_Pnt p2, Standard_Real angle) {
		Standard_Real x1 = p1.X(), x2 = p2.X(), y1 = p1.Y(), y2 = p2.Y();
		Standard_Real b = sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
		Standard_Real rAngle = (angle * M_PI) / 180;
		Standard_Real r = abs(b / (2 * Cos(rAngle)));
		Standard_Real h = sqrt(r * r - (b / 2) * (b / 2));

		Standard_Real x01 = x1 + (x2 - x1) / 2 + h * (y2 - y1) / b;
		Standard_Real y01 = y1 + (y2 - y1) / 2 - h * (x2 - x1) / b;
		Standard_Real x02 = x1 + (x2 - x1) / 2 - h * (y2 - y1) / b;
		Standard_Real y02 = y1 + (y2 - y1) / 2 + h * (x2 - x1) / b;

		Standard_Real oX, oY;

		if (abs(angle) > 180){
			if (((x2 - x1)*(y01 - y1) - (y2 - y1)*(x01 - x1)) < 0){
				oX = x01;
				oY = y01;
			} else {
				oX = x02;
				oY = y02;
			}
		} else {
			if (((x2 - x1)*(y01 - y1) - (y2 - y1)*(x01 - x1)) > 0){
				oX = x01;
				oY = y01;
			}
			else {
				oX = x02;
				oY = y02;
			}
		}
		gp_Pnt circleCenter(oX, oY, 0);
		gp_Dir neckAxis = gp::DZ();
		gp_Ax2 neckAx2(circleCenter, neckAxis);
		gp_Circ circ(neckAx2, r);
	//	CPLogger::Instance()->myfile << "radius " << r << " angle " << angle << " x0 " << x01 << " y0 " << y01 << endl;
		//CPLogger::Instance()->myfile << "radius " << r << " angle " << angle << " x02 " << x02 << " y02 " << y02 << endl;
		Handle(Geom_TrimmedCurve) aArcOfCircle;
		if (angle > 0){
			return GC_MakeArcOfCircle(circ, p1, p2, true);
		}
		else {
			return GC_MakeArcOfCircle(circ, p2, p1, true);
		}
	}


};