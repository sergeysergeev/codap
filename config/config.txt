42
������� ����������
--nozzle-diameter
NumericUpDown
2 0.5 0.1 -1 4
����� ��� z (��)
--z-offset
NumericUpDown
1 0.0 0.1 0 250
������� ��������� (��)
--filament-diameter
NumericUpDown
2 3 0.1 1 100
����������� ���������
--temperature
NumericUpDown
0 200 1 0 600
����������� ��������� (1 ����)
--first-layer-temperature
NumericUpDown
0 200 1 0 600
����������� �����
--bed-temperature
NumericUpDown
0 78 1 0 100
����������� ����� (1 ����)
--first-layer-bed-temperature
NumericUpDown
0 78 1 0 100
�������� �����������
--travel-speed
NumericUpDown
0 130 1 0 6000
�������� ������ ��������� (��/�)
--perimeter-speed
NumericUpDown
0 30 1 0 6000
�������� ������ ���������� ��������� (��/�)
--small-perimeter-speed
NumericUpDown
0 30 1 0 6000
�������� �������� ��������� (��/�)
--external-perimeter-speed
NumericUpDown
0 21 1 0 6000
�������� ���������� (��/�)
--infill-speed
NumericUpDown
0 60 1 0 6000
�������� ������ �������� ������������ (��/�)
--solid-infill-speed
NumericUpDown
0 60 1 0 6000
�������� ������ ������� ������������ (��/�)
--top-solid-infill-speed
NumericUpDown
0 60 1 0 6000
�������� ����������� ��������� (��/�)
--support-material-speed
NumericUpDown
0 60 1 0 6000
�������� ������ ������ (��/�)
--bridge-speed
NumericUpDown
0 60 1 0 6000
�������� ���������� ������ (��/�)
--gap-fill-speed
NumericUpDown
0 20 1 0 6000
�������� ������ ������� ���� (��/�)
--first-layer-speed
NumericUpDown
0 20 1 0 6000
��������� ������ ��������� (��/�^2)
--perimeter-acceleration
NumericUpDown
0 0 1 0 6000
��������� ���������� (��/�^2)
--infill-acceleration
NumericUpDown
0 0 1 0 6000
��������� ������ ������ (��/�^2)
--bridge-acceleration
NumericUpDown
0 0 1 0 6000
��������� ������ 1 ���� (��/�^2)
--first-layer-acceleration
NumericUpDown
0 0 1 0 6000
������ ���� (��)
--layer-height
NumericUpDown
2 0.3 0.05 0 100
������ ������� ���� (��)
--first-layer-height
NumericUpDown
2 0.35 0.05 0 100
���������� ������� N ����
--infill-every-layers
NumericUpDown
0 1 1 0 100
�������� ������ N ����
--solid-infill-every-layers
NumericUpDown
0 0 1 0 100
���������� ����� � ���������
--perimeters
NumericUpDown
0 3 1 0 100
���������� �������� ����� ������
--top-solid-layers
NumericUpDown
0 3 1 0 100
���������� �������� ����� �����
--bottom-solid-layers
NumericUpDown
0 3 1 0 100
��������� ���������� (%)
--fill-density
NumericUpDown
0 40 1 0 100
���� ����������
--fill-angle
NumericUpDown
0 45 1 0 100
����� �������� (��)
--retract-length
NumericUpDown
0 1 1 0 600
�������� �������� (��/�)
--retract-speed
NumericUpDown
0 30 1 0 600
�������� ����������
--cooling
CheckBox
true 
����������� �������� ����������� (%)
--min-fan-speed
NumericUpDown
0 35 1 0 100
������������ �������� ����������� (%)
--max-fan-speed
NumericUpDown
0 100 1 0 100
�������� ����������� �� ������
--bridge-fan-speed
NumericUpDown
0 100 1 0 100
�������� ���������� �� ���������� ������� ������ ����
--fan-below-layer-time
NumericUpDown
0 50 1 0 6000
��������� ���������� �� ���������� ������� ������ ����
--slowdown-below-layer-time
NumericUpDown
0 30 1 0 6000
����������� �������� ������ (��/�)
--min-print-speed
NumericUpDown
0 10 1 0 6000
��������� ���������� ��� N �����
--disable-fan-first-layers
NumericUpDown
0 1 1 0 6000
��������� ������
--fan-always-on
CheckBox
true

_____________________________ Docs:
������ ������ �������� ���������� ������������ ���������. ����� ������������� ��� �������� �� ��������� ���������.

������ NumericUpDown
--�������� ���������� ������ ��� slicer
NumericUpDown ��� ��������
������ ����� ������� | �� ��������� | ��� | ������� | ��������

������ Combobox
--�������� ���������� ������ ��� slicer
ComboBox ��� ��������
2 ���������� ��������� ����������� ������
������� 1
������� 2

������ CheckBox
--�������� ���������� ������ ��� slicer
CheckBox ��� ��������
false - �������� �� ���������