#include"CPReadExample.h"
#include <exception>


CPReadExample::CPReadExample()
{
}

CPReadExample::~CPReadExample()
{
}

TopoDS_Shape CPReadExample::getFromSTEP()
{
	STEPControl_Reader reader;
	IFSelect_ReturnStatus status = reader.ReadFile(STEP_EXAMPLE_FILE);//
	if (status != IFSelect_RetDone) {
		throw new std::exception();
	}
	reader.TransferRoots();
	TopoDS_Shape aShape = reader.OneShape();
	return aShape;
}

TopoDS_Shape CPReadExample::getFromIGES()
{
	IGESControl_Reader reader;
	IFSelect_ReturnStatus status = reader.ReadFile(IGES_EXAMPLE_FILE);//
	if (status != IFSelect_RetDone) {
		throw new std::exception();
	}
	reader.TransferRoots();
	TopoDS_Shape aShape = reader.OneShape();
	return aShape;
}

TopoDS_Shape CPReadExample::getFromSTL() {
	/*OSD_Path path(STL_EXAMPLE_FILE);
	Standard_Boolean ReturnValue = Standard_True;
	Handle(Poly_Triangulation) aSTLMesh = RWStl::ReadAscii(path);
	Handle(XSDRAWSTLVRML_DataSource) aDataSource = new XSDRAWSTLVRML_DataSource(aSTLMesh);*/

	TopoDS_Shape aShape;
	StlAPI_Reader reader;
	reader.Read(aShape, STL_EXAMPLE_FILE);
	return aShape;
}