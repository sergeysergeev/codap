//#include"CPAbstractRead.h"
#include<STEPControl_Reader.hxx>
#include<StlAPI_Reader.hxx>
#include<IGESControl_Reader.hxx>
#include<BRepMesh_IncrementalMesh.hxx>
#include<StlAPI_Writer.hxx>
#include<BRepPrimAPI_MakeTorus.hxx>
#include<BRepAlgoAPI_Fuse.hxx>
#include<BRepBuilderAPI_MakeEdge.hxx>
#include<BRepBuilderAPI_MakeWire.hxx>
#include<BRepBuilderAPI_MakeFace.hxx>
#include<BRepPrimAPI_MakePrism.hxx>
#include<Geom_TrimmedCurve.hxx>
#include<GC_MakeArcOfCircle.hxx>
#include<BRepAlgoAPI_Cut.hxx>
#include <gp_Circ.hxx>
#include <gp_Ax2.hxx>
#include"CPLogger.h"
//#include"CPSlicer.h" deprecated and removed
#include"CPAppExecute.h"
#include"CPIdf_Reader.h"

class CPRead  {
public:
	CPRead();
	TopoDS_Shape StepRead();
	TopoDS_Shape IgesRead();
	TopoDS_Shape StlRead();
	TopoDS_Shape IdfRead();
	TopoDS_Shape Read();
	Standard_CString getFilename();
	void setFilename(Standard_CString fname);
protected:
	Standard_CString filename;
private:
	void exportTmpStl(TopoDS_Shape);
};