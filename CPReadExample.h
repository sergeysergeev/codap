#pragma once
#include<TopoDS_Shape.hxx>
#include<IGESControl_Reader.hxx>
#include<STEPControl_Reader.hxx>
#include<RWStl.hxx>
#include<BRepMesh_IncrementalMesh.hxx>
#include <XSDRAWSTLVRML_DataSource.hxx>
//#include <XSDRAWSTLVRML_DataSource3D.hxx>
#include<StlAPI_Reader.hxx>

#define STEP_EXAMPLE_FILE "C:/Users/rocketracoon/Downloads/ex.step"
#define IGES_EXAMPLE_FILE "C:/Users/rocketraccoon/Projects/codapoem/format-examples/Jietong Switch PBS-12A green.IGS"
#define STL_EXAMPLE_FILE "C:/Users/rocketraccoon/Projects/codapoem/format-examples/out.stl"
class CPReadExample
{

public:
	CPReadExample();
	~CPReadExample();
	static TopoDS_Shape getFromSTEP();
	static TopoDS_Shape getFromIGES();
	static TopoDS_Shape getFromSTL();

};

