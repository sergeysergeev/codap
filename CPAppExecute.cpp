#include "CPAppExecute.h"

CPAppExecute::CPAppExecute()
{
}


void CPAppExecute::Exec(){
	WinExec("C:/Users/rocketracoon/Downloads/slic3r-mswin-x64-1-2-9a-stable/Slic3r/slic3r-console.exe preprocessed.stl", SW_HIDE);
	//startup((LPCTSTR)"C:/Users/rocketracoon/Downloads/slic3r-mswin-x64-1-2-9a-stable/Slic3r/slic3r-console.exe", (LPWSTR) "preprocessed.stl");
	//system("C:/Users/rocketracoon/Downloads/slic3r-mswin-x64-1-2-9a-stable/Slic3r/slic3r-console.exe preprocessed.stl");
}

void CPAppExecute::PcbPng(){ //extensions/gerber/pcb2gcode.exe --metric --zsafe 2 --zwork -0.06 --offset 0.2 --zchange 25 --mill-feed 100 --mill-speed 5000 --back ex.gbl --output-dir tmp/ --no-export --basename output
	//WinExec("C:/Users/rocketracoon/Downloads/pcb2gcodeGUI-1.3.2-1-win64/pcb2gcodeGUI-1.3.2-1-win64/pcb2gcode.exe --metric --zsafe 2 --zwork -0.06 --offset 0.2 --zchange 25 --mill-feed 100 --mill-speed 5000 --back ex.gbl --output-dir tmp/ --no-export --basename output", SW_SHOW);
	startup("extensions/gerber/pcb2gcode.exe --metric --zsafe 2 --zwork -0.06 --offset 0.2 --zchange 25 --mill-feed 100 --mill-speed 5000 --back ex.gbl --output-dir tmp/ --no-export");
	//spawnl(P_WAIT, "C:/Users/rocketracoon/Downloads/pcb2gcodeGUI-1.3.2-1-win64/pcb2gcodeGUI-1.3.2-1-win64/pcb2gcode.exe", " --metric --zsafe 2 --zwork -0.06 --offset 0.2 --zchange 25 --mill-feed 100 --mill-speed 5000 --back ex.gbl --output-dir tmp/ --no-export --basename output");
}

void CPAppExecute::PcbExec(char command[]){
	startup(command);
}

VOID CPAppExecute::startup(char arg[])
{
	// additional information
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	// set the size of the structures
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	
	wchar_t wtext[1000];
	mbstowcs(wtext, arg, strlen(arg) + 1);//Plus null
	LPWSTR ptr = wtext;
	// start the program up
	if (!CreateProcess(NULL,   // the path
		ptr,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		SW_HIDE,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi             // Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
		))
	{
		throw std::exception("Could not create child process");
	}
	// Close process and thread handles. 
	WaitForSingleObject(pi.hProcess, INFINITE);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}

std::string CPAppExecute::AdditionalArgs(char * filename){
	std::ifstream input;
	input.open(filename,std::ios_base::in);
	std::string line = "", result = "";
	while (std::getline(input, line)){
		result += " " + line;
	}
	input.close();
	return result;
	

}